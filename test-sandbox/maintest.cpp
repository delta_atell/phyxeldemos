// sandbox test file that we use to test all the features of the phyxel engine. there were some other ones, but this one is the most readable.
// this file is NOT a part of the Phyxel Engine
#include <iostream>
#include <chrono>
#include <ctime>
#include <cmath>
#include <cstdlib>
#include "phyxel.hpp"
#include <SFML/Graphics.hpp>

#define CREATE_MATS 0

int main() {
    auto start = std::chrono::high_resolution_clock::now();
    auto end = start;

    phx::Scene scene;
    #if PHX_ENABLE_WORLD_SAVE
    phx::WorldManager world("tmp-22", &scene, 0, 0, 4, 4);
    #endif
    
    srand(0);

    std::cout << "1\n";

    #if CREATE_MATS
    auto air = phx::MaterialsList::addMaterial("air", 1, PHX_MTYPE_GAS, phx::Color(0,0,0,0), 0.01, false, true);
        air->isRemovable = true;
        scene.defaultMaterial = air;
    auto sand = phx::MaterialsList::addMaterial("sand", 4, PHX_MTYPE_POD,  phx::Color(255,225,180), 0.001);
        sand->addColor(phx::Color(255,225,180));
        sand->addColor(phx::Color(245,245,170));
        sand->addColor(phx::Color(245,245,170));
        sand->addColor(phx::Color(245,245,170));
        sand->addColor(phx::Color(245,245,170));
        sand->addColor(phx::Color(190,175,170));
    auto water = phx::MaterialsList::addMaterial("water", 3, PHX_MTYPE_LIQ, phx::Color(50,150,255), 0.01);
        water->resistance = 0.18;
        water->viscosity = 3000;
    auto metal = phx::MaterialsList::addMaterial("unknown metal", 4.9, PHX_MTYPE_SOL, phx::Color(170,160,170), 0.1, false, true);
        metal->resistance = 0.1;
    auto slime = phx::MaterialsList::addMaterial("slime", 4, PHX_MTYPE_LIQ, phx::Color(100,0,125), 0.01);
        slime->viscosity = 6500;
        slime->addColor(phx::Color(130,0,125));
    auto ygas = phx::MaterialsList::addMaterial("Y-gas", 0.9, PHX_MTYPE_GAS, phx::Color(155,255,0,60), 0.01);
    auto foam = phx::MaterialsList::addMaterial("foam powder", 2, PHX_MTYPE_POD, phx::Color(255,255,255), 0.003);
        foam->addColor(phx::Color(255, 255, 255));
        foam->addColor(phx::Color(255, 230, 240));
    auto oil = phx::MaterialsList::addMaterial("oil", 1, PHX_MTYPE_LIQ, phx::Color(100,100,30), 0.05, true);
    auto acid = phx::MaterialsList::addMaterial("acid", 3, PHX_MTYPE_LIQ, phx::Color(100,255,100), 0.001);
        acid->resistance = 0.18;
        acid->addReaction(metal, ygas, ygas, 80, 80, PHX_MIN_TEMP, 100);//512);
        acid->addReaction(foam, acid, ygas, 80, 80, PHX_MIN_TEMP, 400);
        acid->setStateTransitions(PHX_MIN_TEMP, 107, acid, ygas);
    auto lava = phx::MaterialsList::addMaterial("lava", 5, PHX_MTYPE_LIQ, phx::Color(255,200,0),  0.00001);
        lava->viscosity = 7000;
    auto stone = phx::MaterialsList::addMaterial("dark stone", 5, PHX_MTYPE_SOL,  phx::Color(100,100,100), 0.00001, false, true);
    auto gstone = phx::MaterialsList::addMaterial("green dark stone", 5, PHX_MTYPE_SOL, phx::Color(80,140,80), 0.0005, false, true);
    auto vap = phx::MaterialsList::addMaterial("vapour", 0.3, PHX_MTYPE_GAS, phx::Color(230,230,230,70), 0.01);
        //lava->addReaction(water, stone, vap, 0, 0, 0, 4000);
        //lava->addReaction(acid, gstone, ygas, 0, 0, 0, 4000);
        //lava->addReaction(metal, lava, lava, 0, 0, 0, 3000);
        lava->addReaction(sand, lava, air, 0, 0, PHX_MIN_TEMP, 700);
        lava->addReaction(foam, lava, air, 0, 0, PHX_MIN_TEMP, 700);
        //lava->addReaction(oil, lava, vacuum, 0, 0, 0, 700); // fire?
    //auto vacuum = phx::MaterialsList::addMaterial("vacuum", 0.1, PHX_MTYPE_GAS, phx::Color(0,0,0,0), 0);
    //vacuum->isRemovable = true;
    auto liqmetal = phx::MaterialsList::addMaterial("liquid metal", 2, PHX_MTYPE_LIQ, phx::Color(250,230,0), 0.05);
        liqmetal->resistance = metal->resistance;
        acid->addReaction(liqmetal, ygas, ygas, 80, 80, PHX_MIN_TEMP, 512);
        metal->setStateTransitions(PHX_MIN_TEMP, 300, metal, liqmetal);
        //liqmetal->setStateTransitions(250, PHX_MAX_TEMP, metal, liqmetal);
        liqmetal->setStateTransitions(260, PHX_MAX_TEMP, metal, liqmetal);
        lava->setStateTransitions(700, PHX_MAX_TEMP, stone, lava);
        stone->setStateTransitions(PHX_MIN_TEMP, 700, stone, lava);
    auto ice = phx::MaterialsList::addMaterial("ice", 3, PHX_MTYPE_SOL, phx::Color(180,180,255), 0.01);
        ice->resistance = 0.34;
        water->setStateTransitions(-2, 107, ice, vap);
        ice->setStateTransitions(PHX_MIN_TEMP, 2, ice, water);
        vap->setStateTransitions(93, PHX_MAX_TEMP, water, vap);
    auto fgas = phx::MaterialsList::addMaterial("F-gas", 5, PHX_MTYPE_GAS, phx::Color(200,0,250,60), 0.01);
    slime->setStateTransitions(PHX_MIN_TEMP, 120, slime, fgas);
    //auto oil_on_fire = phx::MaterialsList::addMaterial("oil(on fire)", 1, PHX_MTYPE_LIQ, 0.05, phx::Color(255,205,30));
    auto co2 = phx::MaterialsList::addMaterial("CO2", 0.5, PHX_MTYPE_GAS, phx::Color(100,100,100,70), 0.05);
    //auto lfire = phx::MaterialsList::addMaterial("fire", 0, PHX_MTYPE_GAS, 0.1, phx::Color(255,150,0,100));
        //oil->addReaction(air, lfire, co2, 800, 200, 200, 3000);
        //oil_on_fire->addReaction(oil, oil_on_fire, oil_on_fire, 10, 10, 0, 4000);
        //lfire->addReaction(oil, oil_on_fire, oil_on_fire, 200, 200, 20, 8191);
        //oil_on_fire->addReaction(air, co2, co2, 10, 10, 0, 1000);
    //auto bedrock = phx::MaterialsList::addMaterial("bedrock", 0, PHX_MTYPE_SOL, phx::Color(0,0,255,255), 0);
    

    std::cout << "2\n";

    air->containsO2 = true;
    oil->isFlammable = true;
    oil->burningFrames = 10;
    oil->burningTemperature = 200;
    oil->burningGas = co2;
    oil->burntState = oil;
    oil->addFireColor(phx::Color(255, 200, 0, 120));
    oil->addFireColor(phx::Color(255, 150, 0, 120));
    oil->addFireColor(phx::Color(255, 150, 0, 120));
    oil->addFireColor(phx::Color(255, 150, 0, 120));
    oil->addFireColor(phx::Color(255, 80, 0, 120));
    oil->addFireColor(phx::Color(255, 80, 0, 120));

    foam->isFlammable = true;
    foam->burningFrames = 15;
    foam->burningTemperature = 200;
    foam->burningGas = co2;
    foam->burntState = foam;
    foam->addFireColor(phx::Color(255, 30, 0, 120));

    world.fillIfEmpty(air, 22.7);
    #else
    #if PHX_ENABLE_WORLD_SAVE
    try {
        world.load();
        std::cout << "world loaded\n";
    } catch (const std::string& s) {
        std::cerr << s << '\n';
    }
    #else
    static_assert(false);
    #endif
    phx::MaterialObj air;
    phx::MaterialObj sand;
    phx::MaterialObj water;
    phx::MaterialObj metal;
    phx::MaterialObj slime;
    phx::MaterialObj ygas;
    phx::MaterialObj foam;
    phx::MaterialObj oil;
    phx::MaterialObj acid;
    phx::MaterialObj lava;
    phx::MaterialObj stone;
    phx::MaterialObj gstone;
    phx::MaterialObj vap;
    phx::MaterialObj liqmetal;
    phx::MaterialObj ice;
    phx::MaterialObj fgas;
    phx::MaterialObj co2;
    try {
        air = phx::MaterialsList::get(1);
        sand = phx::MaterialsList::get(2);
        water = phx::MaterialsList::get(3);
        metal = phx::MaterialsList::get(4);
        slime = phx::MaterialsList::get(5);
        ygas = phx::MaterialsList::get(6);
        foam = phx::MaterialsList::get(7);
        oil = phx::MaterialsList::get(8);
        acid = phx::MaterialsList::get(9);
        lava = phx::MaterialsList::get(10);
        stone = phx::MaterialsList::get(11);
        gstone = phx::MaterialsList::get(12);
        vap = phx::MaterialsList::get(13);
        liqmetal = phx::MaterialsList::get(14);
        ice = phx::MaterialsList::get(15);
        fgas = phx::MaterialsList::get(16);
        co2 = phx::MaterialsList::get(17);
    } catch (const std::string& s) {
        std::cerr << s << '\n';
    }
    #endif

    std::cout << "2.3\n";

    float init_temperatures[] = {
        2, 3, 3, -1, 1, 1, 2, 5, 2, 1200, 2, 2, 500, 0
    };
    auto myBurningRoutine = [](phx::Scene* scene, unsigned X, unsigned Y) {
        auto& phyx = (*scene)(X, Y);
        auto m = (/*phyx.isRigidFree*/ true ? phyx.getMaterial() : NULL /*phyx.coveringRigid->material*/);
        int rnd = rand();
        if (m->fireColors.size() && !(rnd%2)) {
            auto flame = scene->addParticle(NULL, m->getFireColor(rnd%(m->fireColors.size())), X, Y, 0, -1.5, false, true, true);
            flame->customUpdate = [rnd](phx::Particle* self, void*) {
                if ((self->X > PHX_SCENE_SIZE_X || self->X < 0 || self->Y < 0 || self->Y > PHX_SCENE_SIZE_Y) || (self->X+0.5*self->Vx > PHX_SCENE_SIZE_X || self->X+0.5*self->Vx < 0 || self->Y+0.5*self->Vy < 0 || self->Y+0.5*self->Vy > PHX_SCENE_SIZE_Y) || self->lifetime > 5+rnd%10) {
                    self->toDelete = true;
                    return true;
                }
                if (self->color.a <= 10)
                    self->color.a = 0;
                else
                    self->color.a -= 0.05*255;
                return false;
            };
        }
    };

    std::cout << "2.4\n";
    oil->customBurning = myBurningRoutine;
    foam->customBurning = myBurningRoutine;

    std::cout << "2.5\n";

    #if PHX_ENABLE_WORLD_SAVE==0 || CREATE_MATS
    scene.envTemperature = 22.7; // SAVE
    scene.fill(air);
    scene.fillTemperature(22.7);
    #endif

    unsigned frame = 0;
    char materialID = 0;
    bool mousePressed = false;
    bool play = false;
    uint16_t mode = 1; // 0 - temperature, 1 - normal, 2 - set particles

    float coef = 127.5/PHX_MAX_TEMP;
    float half_alpha = 127.5;
    float radToDeg = 180 / M_PI;

    sf::RenderWindow window(sf::VideoMode(PHX_SCENE_SIZE_X, PHX_SCENE_SIZE_Y), "PhyxelEngine 1.3");
    sf::RectangleShape pixelRenderer(sf::Vector2f(1, 1));

    // auto particle_destroy_when_out = [](phx::Particle* self, phx::Scene*)->bool {
    //     if ((self->X >= PHX_SCENE_SIZE_X || self->X < 0 || self->Y < 0 || self->Y >= PHX_SCENE_SIZE_Y)
    //      ||
    //      (self->X+0.5*self->Vx > PHX_SCENE_SIZE_X || self->X+0.5*self->Vx < 0 || self->Y+0.5*self->Vy < 0 || self->Y+0.5*self->Vy >= PHX_SCENE_SIZE_Y)) {
    //         self->toDelete = true;
    //         return true; // stop the update
    //     }
    //     return false;
    // };
    auto particle_destroy_timer_no_custom_fields = [](phx::Particle* self, phx::Scene*) {
        if ((self->X > PHX_SCENE_SIZE_X || self->X < 0 || self->Y < 0 || self->Y > PHX_SCENE_SIZE_Y) || (self->X+0.5*self->Vx > PHX_SCENE_SIZE_X || self->X+0.5*self->Vx < 0 || self->Y+0.5*self->Vy < 0 || self->Y+0.5*self->Vy > PHX_SCENE_SIZE_Y) || self->lifetime > 1500) {
            self->toDelete = true;
            return true; // stop the update
        }
        self->color.a = 255-255*(self->lifetime)/1500;
        return false;
    };
    auto particle_destructor_test = [](phx::Particle* self, phx::Scene* scene) {
        // scene.setMaterial(self->X, self->Y, water);
        // scene.setColor(self->X, self->Y, water->getColor(rand()%(water->colors.size())));
        // scene.setTemperature(scene.getSizeX()*(int)(self->Y)+(int)(self->X), PHX_MAX_TEMP);
    };

    phx::RigidBody* loopRigidTest = nullptr;
    unsigned loopRigidTestID = 0;
    try {
        // loopRigidTest = scene.addRigidBody(2, 60, 3, 3, metal);
        // loopRigidTestID = loopRigidTest->getID();
    } catch (const std::string& s) {
        std::cerr << s << '\n';
    }
    int testForceDir = 0; // force to apply on a test body
    b2Vec2 forcesApp[4] = {
        b2Vec2(0, -20),
        b2Vec2(-20, -2),
        b2Vec2(0, 20),
        b2Vec2(20, -2)
    };

    //linkRigidBodies(loopRigidTest, loopRigidTest1);
    //linkRigidBodies(loopRigidTest2, loopRigidTest1);

    #if CREATE_MATS
    scene.makeRectangle(sand, 32, 32, 96, 96);
    #endif

    std::cout << "3\n";

    while (window.isOpen()) {
        window.setFramerateLimit(30);
        if (play && loopRigidTest && scene.exists(loopRigidTest, loopRigidTestID)) {
            std::cout << "body: " << loopRigidTest->getID() << ' ' << loopRigidTest->toDelete << '\n';
            std::cout << loopRigidTest->getX() << ' ' << loopRigidTest->getY() << '\n'; 
        }

        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed)
                window.close();
            else if (event.type == sf::Event::KeyPressed) {
                if (sf::Keyboard::Num0 <= event.key.code && event.key.code <= sf::Keyboard::Num9)
                    materialID = event.key.code - sf::Keyboard::Num0 + 1;
                else if (sf::Keyboard::Space == event.key.code)
                    play = !play;
                else if (sf::Keyboard::S == event.key.code && !play) // single update step
                    scene.updateAll();
                else if (sf::Keyboard::T == event.key.code) { // temperature
                    mode = !mode;
                } else if (sf::Keyboard::P == event.key.code) { // particles
                    mode = 2;
                } else if (sf::Keyboard::E == event.key.code) { // electricity
                    if(mode == 3)
                        mode = 1;
                    else
                        mode = 3;
                } else if (sf::Keyboard::B == event.key.code) { // bodies
                    if(mode == 4)
                        mode = 1;
                    else
                        mode = 4;
                } else if (sf::Keyboard::F == event.key.code) { // force (on rigids)
                    if(mode == 5)
                        mode = 1;
                    else
                        mode = 5;
                } else if (sf::Keyboard::D == event.key.code) { // debug mode
                    if(mode == 6)
                        mode = 1;
                    else
                        mode = 6;
                } else if (sf::Keyboard::Z == event.key.code) {
                    world.repositionScene(!world.getSceneUpperLeftChunkX(), !world.getSceneUpperLeftChunkY());
                }  else if (mode == 5) {
                     if (scene.exists(loopRigidTest, loopRigidTest->getID())) {
                         if (sf::Keyboard::Up == event.key.code) {
                             testForceDir = 0;
                             auto c = loopRigidTest->box2DBody->GetPosition();
                             loopRigidTest->box2DBody->ApplyForce(forcesApp[testForceDir], loopRigidTest->box2DBody->GetWorldCenter(), false);
                         } else if (sf::Keyboard::Left == event.key.code) {
                             testForceDir = 1;
                             auto c = loopRigidTest->box2DBody->GetPosition();
                             loopRigidTest->box2DBody->ApplyForce(forcesApp[testForceDir], loopRigidTest->box2DBody->GetWorldCenter(), false);
                         } else if (sf::Keyboard::Down == event.key.code) {
                             testForceDir = 2;
                             auto c = loopRigidTest->box2DBody->GetPosition();
                             loopRigidTest->box2DBody->ApplyForce(forcesApp[testForceDir], loopRigidTest->box2DBody->GetWorldCenter(), false);
                         } else if (sf::Keyboard::Right == event.key.code) {
                             testForceDir = 3;
                             auto c = loopRigidTest->box2DBody->GetPosition();
                             loopRigidTest->box2DBody->ApplyForce(forcesApp[testForceDir], loopRigidTest->box2DBody->GetWorldCenter(), false);
                         }
                     }
                }
            } else if (event.type == sf::Event::MouseButtonPressed) {
                if (sf::Mouse::Left == event.key.code)
                    mousePressed = true;
            } else if (event.type == sf::Event::MouseButtonReleased) {
                if (sf::Mouse::Left == event.key.code)
                    mousePressed = false;
            }
        }

        if (mousePressed) {
            auto mousePos = sf::Mouse::getPosition(window);
            auto windowSize = window.getSize();
            float y = mousePos.y/(float)windowSize.y*PHX_SCENE_SIZE_Y;
            float x = mousePos.x/(float)windowSize.x*PHX_SCENE_SIZE_X;
            auto m = phx::MaterialsList::get(materialID);
            if (mode == 2) {
                /// non-physical test
                float v = 0.9;
                auto rnd = rand();
                
                 auto p = scene.addParticle(m, m->getColor(rand()%(m->colors.size())), x, y, 2, -5, true);
                 //p->customUpdate = particle_destroy_when_out;
                // p->isBurning = true;
            } else if (mode == 3) {
                scene.setCharge(x, y, 1);
            } else if (mode == 4) {
                try {
                    if (m->type != PHX_MTYPE_GAS) auto rigObj = scene.addRigidBody(x, y, 3, 3, m);
                } catch (const std::string & s) {
                    std::cout << s << '\n';
                }
            } else if (mode == 5) {
                // auto c = loopRigidTest->box2DBody->GetPosition();
                // //float norm = sqrt((c.x - x)*(c.x - x) + (c.y - y)*(c.y - y));
                // loopRigidTest->box2DBody->ApplyForce(forcesApp[testForceDir], loopRigidTest->box2DBody->GetWorldCenter(), false);
            } else if (mode == 6) {
                // debug mode
                auto& phyx = scene((unsigned)x, (unsigned)y);
                std::cout << "\n=============\n";
                std::cout << "x y:" << (unsigned)x << " " << (unsigned)y << '\n';
                std::cout << "@:" << &phyx << '\n';
                std::cout << "material: " << phyx.getMaterial()->name << '\n';
                std::cout << "temp: " << phyx.getTemperature() << '\n';
                std::cout << "charge: " << phyx.getCharge() << '\n';
                std::cout << "lockmask: " << (unsigned)phyx.getLockMask() << '\n';
                std::cout << "burning stg: " << (unsigned)phyx.getBurningStage() << '\n';
                std::cout << "nbr(d,r,u,l): " << phyx.getNbrDown()->getMaterial()->name << ' ' << phyx.getNbrRight()->getMaterial()->name << ' ' << phyx.getNbrUp()->getMaterial()->name << ' ' << phyx.getNbrLeft()->getMaterial()->name << '\n';
                //std::cout << "" <<  << '\n';
            } else if (mode) {
                scene.setMaterial(x, y, m, rand());
                scene.setTemperature(x, y, init_temperatures[materialID-1]);
            } else {
                scene.setTemperature(x, y, (materialID-1 ? PHX_MAX_TEMP : PHX_MIN_TEMP));
            }
        }


        if (play) {
            start = std::chrono::high_resolution_clock::now();
            auto duration1 = std::chrono::duration_cast<std::chrono::microseconds>(start - end);
            start = std::chrono::high_resolution_clock::now();
            scene.updateAllParallel(4);
            end = std::chrono::high_resolution_clock::now();
            auto duration2 = std::chrono::duration_cast<std::chrono::microseconds>(end-start);
            std::cout << "UpdateAll: " << duration2.count() / (double)(duration1.count() + duration2.count()) * 100 << "%" << std::endl;
            end = std::chrono::high_resolution_clock::now();
        }

        // draw
        window.clear();
        
        if (mode) {
            for (unsigned i = 0; i < PHX_SCENE_SIZE_X*PHX_SCENE_SIZE_Y; ++i) {
                phx::Color c;
                if (scene.getCharge(i%PHX_SCENE_SIZE_X, i/PHX_SCENE_SIZE_X) > 0) c = phx::Color(255,255,255);
                // if (scene[i].box2DBody) c = phx::Color(0,80,255);
                else c = scene.getColor(i%PHX_SCENE_SIZE_X, i/PHX_SCENE_SIZE_X);
                pixelRenderer.setFillColor(sf::Color(c.r, c.g, c.b, c.a));
                pixelRenderer.setPosition(i%PHX_SCENE_SIZE_X, i/PHX_SCENE_SIZE_X);
                window.draw(pixelRenderer);
            }
            
            for (auto p : scene.particles) {
                auto c = p->color;
                pixelRenderer.setFillColor(sf::Color(c.r, c.g, c.b, c.a));
                pixelRenderer.setPosition(p->X, p->Y);
                window.draw(pixelRenderer);
            }
            pixelRenderer.setFillColor(sf::Color(255,255,255));
            
            for (auto b : scene.rigidBodies) {
                auto c = b->color;
                pixelRenderer.setFillColor(sf::Color(c.r, c.g, c.b, c.a));
                if (mode != 4 && mode != 5)
                    for (const auto & coord : b->coveredPhyxels) {
                        if (scene.getCharge(coord.first, coord.second) > 0) c = phx::Color(255,255,255);
                        else c = b->color;
                        pixelRenderer.setFillColor(sf::Color(c.r, c.g, c.b, c.a));
                        pixelRenderer.setPosition(coord.first, coord.second);
                        window.draw(pixelRenderer);
                    }
                else {
                    auto p = b->box2DBody->GetPosition();
                    pixelRenderer.setRotation(b->getAngle()*radToDeg);
                    float cs = cos(b->getAngle());
                    float sn = sin(b->getAngle());
                    pixelRenderer.setPosition(p.x*PHX_METERS2PHYXELS-0.5*(cs*b->getSizeX()-sn*b->getSizeY()), p.y*PHX_METERS2PHYXELS-0.5*(sn*b->getSizeX()+cs*b->getSizeY()));
                    pixelRenderer.setSize(sf::Vector2f(b->getSizeX(), b->getSizeY()));
                    window.draw(pixelRenderer);
                }
            }

             if (loopRigidTest && scene.exists(loopRigidTest, loopRigidTest->getID())) {
                 pixelRenderer.setRotation(0.f);
                 pixelRenderer.setSize(sf::Vector2f(1, 1));
                 for (const auto & coord : loopRigidTest->framePhyxels) {
                     phx::Color c = phx::Color(240,40,0);
                     pixelRenderer.setFillColor(sf::Color(c.r, c.g, c.b, c.a));
                     pixelRenderer.setPosition(coord.first, coord.second);
                     window.draw(pixelRenderer);
                 }
             }

             pixelRenderer.setRotation(0.f);
             pixelRenderer.setFillColor(sf::Color(150, 150, 0));

             pixelRenderer.setSize(sf::Vector2f(1, 1));
        } else {
            for (unsigned i = 0; i < PHX_SCENE_SIZE_X*PHX_SCENE_SIZE_Y; ++i) {
                float tau = scene.getTemperature(i%PHX_SCENE_SIZE_X, i/PHX_SCENE_SIZE_X)-PHX_MIN_TEMP;
                pixelRenderer.setFillColor(sf::Color(half_alpha+coef*tau, half_alpha+coef*tau, 255, coef*tau));
                pixelRenderer.setPosition(i%PHX_SCENE_SIZE_X, i/PHX_SCENE_SIZE_X);
                window.draw(pixelRenderer);
            }
        }

        pixelRenderer.setPosition(0,0);

        window.display();
        ++frame;
    }

    #if PHX_ENABLE_WORLD_SAVE
    world.save();
    #endif

    return 0;
}
